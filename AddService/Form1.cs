﻿using Share;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddService
{

    public partial class Form1 : Form
    {
        string pathToFile = "";//to save the location of the selected object
        string text = "";
        OpenFileDialog openFileDialog1;


        public Form1()
        {
            InitializeComponent();
            button1.Visible = false;
            label4.Visible = false;
            label6.Visible = false;
            textBox2.Visible = false;
            label5.Visible = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            if (comboBox1.Text.Equals("Corba IDL"))
                openFileDialog1.Filter = "IDL files (*.idl)|*.idl";
            if (comboBox1.Text.Equals("RMI JAVA"))
                openFileDialog1.Filter = "rmi interface (*.java)|*.java";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pathToFile = openFileDialog1.FileName;
                label6.Text = openFileDialog1.SafeFileName;
                Console.WriteLine(pathToFile);
            }
            if (File.Exists(pathToFile))
            {
                using (StreamReader sr = new StreamReader(pathToFile))
                {
                    text = sr.ReadToEnd();
                }
            }
            RMIEnitity asd = new RMIEnitity(text);
            Console.WriteLine(asd.nameInterface[0]);
            Console.WriteLine(asd.nameMethod[0]);
        }
        private void button2_Click(object sender, EventArgs e)
        {
             openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = "c:\\";
                if (comboBox1.Text.Equals("Corba IDL"))
                    openFileDialog1.Filter = "IDL files (*.idl)|*.idl";
                if (comboBox1.Text.Equals("RMI JAVA"))
                    openFileDialog1.Filter = "rmi interface (*.java)|*.java";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pathToFile = openFileDialog1.FileName;
                    label6.Text = openFileDialog1.SafeFileName;
                    Console.WriteLine(pathToFile);
                }
                if (File.Exists(pathToFile))
                {
                    using (StreamReader sr = new StreamReader(pathToFile))
                    {
                        text = sr.ReadToEnd();
                    }
                }
        }

        private void VisibleFalseAll()
        {

            label6.Visible = false;
            button1.Visible = false;
            label4.Visible = false;

            textBox2.Visible = false;
            label5.Visible = false;
        }
        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
