﻿using Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share
{
    [Serializable]
    public class CorbaEntity : service
    {
        public CorbaEntity()
        {
        }
        public CorbaEntity(string dataFile)
        {
            this.dataFile = dataFile;
        }
        public string dataFile { get; set; }
    }
}
