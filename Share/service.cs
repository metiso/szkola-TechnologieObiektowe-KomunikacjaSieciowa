﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share
{
    [Serializable]
    public abstract class service
    {
        public string serviceType
        {
            get;
            set;
        }
        public string serviceName
        {
            get;
            set;
        }

        public string serviceDesc
        {
            get;
            set;
        }
        public string operation
        {
            get;
            set;
        }
        public string serverName
        {
            get;
            set;
        }
    }
}
