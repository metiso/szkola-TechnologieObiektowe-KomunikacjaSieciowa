﻿using Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share
{
    [Serializable]
    public class WBEntity : service
    {
        public WBEntity()
        {
        }
        public WBEntity(string argwsdl)
        {
            this.wsdl = argwsdl;
        }
        public string wsdl { get; set; }
    }
}
