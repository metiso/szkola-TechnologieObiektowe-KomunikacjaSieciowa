using System.Net.Sockets;
using System.Net;
using System;
using System.Threading;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using Share;

namespace SerwerServices
{
    public class Program
    {
        public static string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        private Object thisLock = new Object();
        static List<service> myService = new List<service>();
        static void recv(string mcastGroup, string port)
        {
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, int.Parse(port));
            s.Bind(ipep);
            IPAddress ip = IPAddress.Parse(mcastGroup);
            s.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ip, IPAddress.Any));
            while (true)
            {
                byte[] b = new byte[15];
                Console.WriteLine("Oczekiwanie na rozg�os...");
                s.Receive(b);
                string IP_recv = System.Text.Encoding.ASCII.GetString(b, 0, b.Length).Trim();
                Console.WriteLine("");
                Console.WriteLine("Otrzymalem z rozgloszenia IP: " + IP_recv);
                b = new byte[5];
                s.Receive(b);
                string port_recv = System.Text.Encoding.ASCII.GetString(b, 0, b.Length).Trim();
                int int_port = Int32.Parse(port_recv.Trim());
                Console.WriteLine("Otrzymalem z rozgloszenia Port: " + int_port);
                
                Console.WriteLine("Czekam 3 sec na uruchomienie wyslania aktywnych uslug!");
                Thread.Sleep(3 * 1000);
                string p1 = IP_recv;
                int p2 = int_port;
                object args = new object[2] { p1, p2 };

                Thread b1 = new Thread(new ParameterizedThreadStart(Program.SendName));
                b1.Start(args);

            }
        }
        public static void SendName(object args)
        {
            Array argArray = new object[3];
            argArray = (Array)args;
            string p1 = (string)argArray.GetValue(0);
            int p2 = (int)argArray.GetValue(1);
            Console.WriteLine("Uruchomiono wysylanie nazwy do :'{0}','{1}'", p1, p2);
            TcpClient client = new TcpClient(p1, p2);
           
            string myName = Environment.MachineName;
            BinaryFormatter _bFormatter;
            _bFormatter = new BinaryFormatter();
            _bFormatter.Serialize(client.GetStream(), myName);
            _bFormatter = new BinaryFormatter();
            _bFormatter.Serialize(client.GetStream(), GetIPAddress());
            _bFormatter = new BinaryFormatter();
            _bFormatter.Serialize(client.GetStream(), myService);
            client.Close();

            client.Close();
            Console.WriteLine("Zakonczono wysylanie nazwy do :'{0}','{1}'", p1, p2);
        }
        public static void SendServices(object args)
        {
            Array argArray = new object[3];
            argArray = (Array)args;
            string p1 = (string)argArray.GetValue(0);
            int p2 = (int)argArray.GetValue(1);
            Console.WriteLine("Uruchomiono wysylanie listy uslug do :'{0}','{1}'", p1, p2);
            TcpClient client = new TcpClient(p1, p2);
            using (BinaryWriter writer = new BinaryWriter(client.GetStream()))
            {
                writer.Write("LIST");
            }
            BinaryFormatter _bFormatter;
            _bFormatter = new BinaryFormatter();
            _bFormatter.Serialize(client.GetStream(), myService);
            client.Close();
            Console.WriteLine("Zakonczono wysylanie listy uslug do :'{0}','{1}'", p1, p2);
        }
        private void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try
            {
                BinaryFormatter _bFormatter = new BinaryFormatter();
                service service = (service)_bFormatter.Deserialize(client.GetStream());
                if (service.operation == "ADD")
                {
                    service.serverName = Environment.MachineName;
                    myService.Add(service);

                    Console.WriteLine("Dodano Service:" + service.serviceName);
                }
                else if (service.operation == "DELETE")
                {
                    var serviceQuery = myService.Select(n => n)
                    .Where(n => n.serviceName == service.serviceName)
                    .Where(n => n.serviceType == service.serviceType)
                    .ToList();
                    foreach (var services in serviceQuery)
                    {
                        Console.WriteLine("Usunieto Service:" + service.serviceName);
                        myService.Remove(services);
                    }
                }
            }
            catch (IOException)
            {
                Console.WriteLine("Problem z komunikacj� klienta. Zamkni�cie w�tku.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }
        public static void ManagerServices(object args)
        {
            TcpListener listener = null;
            try
            {
                int port1 = 3984;
                listener = new TcpListener(IPAddress.Parse(GetIPAddress()), port1);
                listener.Start();
                Console.WriteLine("Serwer Nasluchu AddService startuje..." + GetIPAddress() + ":" + port1);
                Console.WriteLine("");
                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Zaakceptowano po�aczenie z klientem (Nasluchu AddService)...");
                    new Thread(new Program().ProcessClientRequests).Start(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
        public static void Main(string[] args)
        {
            new Thread(ManagerServices).Start(); 
            recv("224.5.6.7", "5000"); 
        }
    }
}