﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Share;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace ClientForm
{
    public partial class Form1 : Form
    {
        string pathToFile = "";
        string lastReadIp = null;
        string text = "";
        OpenFileDialog openFileDialog1;
        static int Port = 3994;
        static int port2 = 3994;
        static List<service> myService = new List<service>();
        static List<ListViewItem> ListViewItem = new List<ListViewItem>();
        static TcpListener listener = new TcpListener(IPAddress.Parse(GetIPAddress()), port2);
        service lastReadService = null;
        public static string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView3.Items.Clear();
            if (listView1.SelectedItems.Count > 0)
            {
                lastReadService = (service)listView1.SelectedItems[0].Tag;
                if (lastReadService is RMIEnitity)
                {
                    
                    RMIEnitity tmp = (RMIEnitity)lastReadService;
                    int i = 0;
                    foreach (string stringfor in tmp.nameInterface)
                    {
                        ListViewItem item = new ListViewItem();
                         item.Text = stringfor;
                        item.SubItems.Add(tmp.nameMethod[i]);
                        item.SubItems.Add(tmp.argumentMethod[i]);
                        item.Tag = tmp;
                        listView3.Items.Add(item);
                        i++;
                    }


                }

            }
            else
            {
                lastReadService = null;
            }
        }

        public static void send(string mcastGroup, string port, string ttl, string rep, string oper)
        {
            string Port_str = Convert.ToString(Port);
            IPAddress ip;
            try
            {
                Console.WriteLine("Wysylam rozgłos na Grupe IP: {0} Port: {1} TTL: {2}", mcastGroup, port, ttl);
                ip = IPAddress.Parse(mcastGroup);
                Socket s = new Socket(AddressFamily.InterNetwork,
                                SocketType.Dgram, ProtocolType.Udp);
                s.SetSocketOption(SocketOptionLevel.IP,
                    SocketOptionName.AddMembership, new MulticastOption(ip));
                s.SetSocketOption(SocketOptionLevel.IP,
                    SocketOptionName.MulticastTimeToLive, int.Parse(ttl));
                byte[] ip_send = new byte[32];
                ip_send = Encoding.ASCII.GetBytes(GetIPAddress());
                byte[] Port_send = new byte[5];
                Port_send = Encoding.ASCII.GetBytes(Port_str);

                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(mcastGroup), int.Parse(port));
                Console.WriteLine("Łączenie...");
                s.Connect(ipep);
                Console.WriteLine("Wysyłanie mojego ip i portu....");
                s.Send(ip_send, ip_send.Length, SocketFlags.None);
                s.Send(Port_send, Port_send.Length, SocketFlags.None);
                Console.WriteLine("Zakonczenie rozglaszania");
                Console.WriteLine("");
                s.Close();
            }
            catch (System.Exception e)
            { Console.Error.WriteLine(e.Message); }
        }
        private static void ProcessClientRequests(object argument)
        {

            TcpClient client = (TcpClient)argument;
            BinaryFormatter _bFormatter;
            _bFormatter = new BinaryFormatter();
            Console.WriteLine("Dersializacja nazwy.");
            ListViewItem item = new ListViewItem();
            item.Text = (String)_bFormatter.Deserialize(client.GetStream());
            item.SubItems.Add((String)_bFormatter.Deserialize(client.GetStream()));
            List<service> asd = (List<service>)_bFormatter.Deserialize(client.GetStream());
            item.Tag = asd;

            ListViewItem.Add(item);
            
            client.Close();
            Console.WriteLine("Wylaczenie Serwera Webservice.");

        }

        public Form1()
        {
            InitializeComponent();
            VisibleFalseAll();
            listView2.Columns.Add("Nazwa Komputera", 210);
            listView2.Columns.Add("IP", 100);

            listView1.Columns.Add("Nazwa usługi", 120);
            listView1.Columns.Add("Typ usługi", 135);
            listView1.Columns.Add("Opis usługi", 120);

            listView3.Columns.Add("Nazwa Interfejsu", 100);
            listView3.Columns.Add("Nazwa Metody", 100);
            listView3.Columns.Add("Argumenty", 100);
            Thread t = new Thread(Server);
            t.Start();
        }
        void Server()
        {
            try
            {
                Console.WriteLine("Serwer pobierania Services startuje... " + GetIPAddress() + ":" + port2);
                while (true)
                {
                    try
                    {
                        TcpClient client = listener.AcceptTcpClient();
                        Console.WriteLine(">Zaakceptowano połaczenie od Serwera Webservice.");
                        Thread t = new Thread(ProcessClientRequests);
                        t.Start(client);
                    }
                    catch (Exception es)
                    { }
                }
            }
            catch (Exception es)
            { Console.WriteLine(es); }
            finally
            {
                if (listener != null)
                { listener.Stop(); }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ListViewItem = new List<ListViewItem>();
            listView2.Items.Clear(); 
            listener.Start();
            send("224.5.6.7", "5000", "1", "2", "");
            Thread.Sleep(1000 * 5); 
            listener.Stop(); 

            foreach (ListViewItem item in ListViewItem) 
            {
                try
                {
                    listView2.Items.Add(item);
                }
                catch
                {

                }
            }
            
            myService = new List<service>();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lastReadService != null)
            {
                if (lastReadService.serviceType.Equals("WebService"))
                {
                    WBEntity wb = (WBEntity)lastReadService;
                    NotepadHelper.ShowMessage(wb.wsdl, wb.serverName + ":" + wb.serviceName);
                }
                else if (lastReadService.serviceType.Equals("Corba IDL"))
                {
                    CorbaEntity wb = (CorbaEntity)lastReadService;
                    Stream myStream;
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "idl files (*.idl)|*.idl";
                    saveFileDialog1.FilterIndex = 1;
                    saveFileDialog1.RestoreDirectory = true;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        if ((myStream = saveFileDialog1.OpenFile()) != null)
                        {
                            byte[] bytes = Encoding.ASCII.GetBytes(wb.dataFile);
                            myStream.Write(bytes, 0, bytes.Length);
                            myStream.Close();
                        }
                    }
                }
                else if (lastReadService.serviceType.Equals("RMI JAVA"))
                {

                    RMIEnitity wb = (RMIEnitity)lastReadService;
                    Stream myStream;
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "java files (*.java)|*.java";
                    saveFileDialog1.FilterIndex = 1;
                    saveFileDialog1.RestoreDirectory = true;

                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        if ((myStream = saveFileDialog1.OpenFile()) != null)
                        {
                            byte[] bytes = Encoding.ASCII.GetBytes(wb.dataFile);
                            myStream.Write(bytes, 0, bytes.Length);
                            myStream.Close();
                        }
                    }
                }
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            string textWyszukania = textBox1.Text;
            List<String> result = new List<String>();
            Regex rgx = new Regex("(?i)" + textWyszukania);

            foreach (ListViewItem item in ListViewItem)
            {
                if (rgx.IsMatch(item.Text))
                {
                    result.Add("Computer : " + item.Text);
                }

                List<service> ServicesList = (List<service>)item.Tag;
                foreach (service servic in ServicesList)
                {
                    if (rgx.IsMatch(servic.serviceName))
                    {
                        result.Add("Computer : " + item.Text + " > Service Name : " + servic.serviceName);
                    }
                    if (rgx.IsMatch(servic.serviceDesc))
                    {
                        result.Add("Computer : " + item.Text + " > Service  Desc: " + servic.serviceDesc);
                    }

                    if (rgx.IsMatch(servic.serviceType))
                    {
                        result.Add("Computer : " + item.Text + " > Service Type: " + servic.serviceType);
                    }
                    if (servic is RMIEnitity)
                    {
                        RMIEnitity RE = (RMIEnitity)servic;
                        if (rgx.IsMatch(RE.nameInterface[0]))
                        {
                            result.Add("Computer : " + item.Text + " > Service : " + servic.serviceName + " > RMI Name Interface : " + RE.nameInterface[0]);
                        }
                        for (int i = 0; i < RE.nameMethod.Count; i++)
                        {
                            if (rgx.IsMatch(RE.nameMethod[i]))
                            {
                                result.Add("Computer : " + item.Text + " > Service : " + servic.serviceName + " > RMI " + RE.nameInterface[i] + "> Name method : " + RE.nameMethod[i]);
                           
                            }
                        }
                        for (int i = 0; i < RE.argumentMethod.Count;i++ )
                        {
                            if (rgx.IsMatch(RE.argumentMethod[i]))
                            {
                                result.Add("Computer : " + item.Text + " > Service : " + servic.serviceName + " > RMI " + RE.nameInterface[i] + "> Name method : " + RE.nameMethod[i] +"> argument : " + RE.argumentMethod[i]);
                            }
                        }
                           
                    }
                }

            }
            string resul = "";
            foreach (string str in result)
            {
                resul = resul + str + "\n";
            }
            if (resul == "")
            {
                resul = "Brak rezultatów";
            }
            MessageBox.Show(resul);

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!comboBox2.Text.Equals("") && !textBox4.Text.Equals("") && (lastReadIp != null))
            {
                bool flag = false; ;
                service service = null;
                switch (comboBox2.Text)
                {
                    case "Corba IDL":
                        if ((text == "") || !(Path.GetExtension(pathToFile).Equals(".idl")))
                        { MessageBox.Show("Nie zaznaczono pliku lub wybrany zly format"); break; }
                        service = new CorbaEntity(text);
                        flag = true; ;
                        break;
                    case "RMI JAVA":
                        if ((text == "") || !(Path.GetExtension(pathToFile).Equals(".java")))
                        { MessageBox.Show("RMIJAVA Nie zaznaczono pliku lub wybrany zly format"); break; }
                        service = new RMIEnitity(text);
                        flag = true; ;
                        break;
                    case "WebService":
                        var expression = "\\b(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[A-Z0-9+&@#/%=~_|]";


                        if ((textBox3.Text.Equals("")) || !FullMatch(textBox3.Text, expression, RegexOptions.IgnoreCase))
                        { 
                            MessageBox.Show("Nie wpisano adresu lub bledny adres"); 
                            break; 
                        }

                        service = new WBEntity(textBox3.Text);
                        flag = true; ;
                        break;
                    default:
                        flag = false; ;
                        break;
                }

                if (flag == true)
                {
                    service.serviceName = textBox4.Text;
                    service.serviceType = comboBox2.Text;
                    service.serviceDesc = textBox2.Text;
                    service.operation = "ADD";

                    if (listView2.SelectedItems.Count > 0)
                    {
                        List<service> tmp = (List<service>)listView2.SelectedItems[0].Tag;
                        tmp.Add(service);
                        listView2.SelectedItems[0].Tag = tmp;
                        listView1.Items.Clear();
                        foreach (service servicee in tmp) 
                        {
                            ListViewItem item = new ListViewItem();
                            item.Text = servicee.serviceName;
                            item.SubItems.Add(servicee.serviceType);
                            item.SubItems.Add(servicee.serviceDesc);
                            item.Tag = servicee;
                            listView1.Items.Add(item);

                        }
                    }
                    flag = false;

                    TcpClient client = new TcpClient(lastReadIp, 3984);
                    BinaryWriter writer = new BinaryWriter(client.GetStream());
                    BinaryFormatter _bFormatter;
                    _bFormatter = new BinaryFormatter();
                    _bFormatter.Serialize(client.GetStream(), service);

                    label8.Text = "Name File";
                    client.Close();
                    textBox4.Text = "";
                    textBox2.Text = "";
                    pathToFile = "";
                    text = "";
                    MessageBox.Show("Wysłano do dodania");
                }
            }
            else
            {
                MessageBox.Show("Wpisano danych lub nie zaznaczono serwera");
            }
        }
        private bool FullMatch(string input, string pattern, RegexOptions options)
        {
            Match match = Regex.Match(input, pattern, options);

            return match.Success && match.Length == input.Length;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (!(comboBox2.Text.Equals("")) && !comboBox2.Text.Equals("WebService"))
            {
                openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = "c:\\";
                if (comboBox2.Text.Equals("Corba IDL"))
                    openFileDialog1.Filter = "IDL files (*.idl)|*.idl";
                if (comboBox2.Text.Equals("RMI JAVA"))
                    openFileDialog1.Filter = "rmi interface (*.java)|*.java";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pathToFile = openFileDialog1.FileName;
                    label8.Text = openFileDialog1.SafeFileName;
                    Console.WriteLine(pathToFile);
                }
                if (File.Exists(pathToFile))
                {
                    using (StreamReader sr = new StreamReader(pathToFile))
                    {
                        text = sr.ReadToEnd();
                    }
                }
                else
                {
                    text = "";
                }
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView3.Items.Clear();
            if (listView2.SelectedItems.Count > 0)
            {

                lastReadIp = (string)listView2.SelectedItems[0].SubItems[1].Text;
                List<service> ServicesList = (List<service>)listView2.SelectedItems[0].Tag;
                foreach (service service in ServicesList)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = service.serviceName;
                    item.SubItems.Add(service.serviceType);
                    item.SubItems.Add(service.serviceDesc);
                    item.Tag = service;
                    listView1.Items.Add(item);

                }


            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            service service = null;
            bool flag = false;
            switch (listView1.SelectedItems[0].SubItems[1].Text)
            {

                case "Corba IDL":
                    service = new CorbaEntity("");
                    flag = true; ;
                    break;
                case "RMI JAVA":
                    service = new RMIEnitity();
                    flag = true; ;
                    break;
                case "WebService":
                    service = new WBEntity("");
                    flag = true; ;
                    break;
                default:
                    flag = false; ;
                    break;
            }
            if (flag == true)
            {
                service.serviceName = listView1.SelectedItems[0].Text;
                service.serviceType = listView1.SelectedItems[0].SubItems[1].Text;
                service.operation = "DELETE";


                if (listView2.SelectedItems.Count > 0)
                {
                    List<service> tmp = (List<service>)listView2.SelectedItems[0].Tag;
                    var serviceQuery = tmp.Select(n => n)
                   .Where(n => n.serviceName == service.serviceName)
                   .Where(n => n.serviceType == service.serviceType)
                   .ToList();
                    foreach (var services in serviceQuery)
                    {
                        tmp.Remove(services);
                    }
                    listView1.Items.Clear();
                    foreach (service servicee in tmp)
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = servicee.serviceName;
                        item.SubItems.Add(servicee.serviceType);
                        item.SubItems.Add(servicee.serviceDesc);
                        item.Tag = servicee;
                        listView1.Items.Add(item);

                    }
                }

                TcpClient client = new TcpClient(lastReadIp, 3984);
                BinaryWriter writer = new BinaryWriter(client.GetStream());
                BinaryFormatter _bFormatter;
                _bFormatter = new BinaryFormatter();
                _bFormatter.Serialize(client.GetStream(), service);
                client.Close();
                textBox1.Text = "";
                textBox3.Text = "";
                pathToFile = "";
                MessageBox.Show("Wysłano do usunięcia");

            }

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text.Equals("Corba IDL") || comboBox2.Text.Equals("RMI JAVA"))
            {
                button5.Visible = true;
                label4.Visible = true;
                label8.Visible = true;
                textBox3.Visible = false;
                label5.Visible = false;
            }
            else if (comboBox2.Text.Equals("WebService"))
            {
                button5.Visible = false;
                label4.Visible = false;
                label8.Visible = false;
                textBox3.Visible = true;
                label5.Visible = true;
            }
            else
            {
                VisibleFalseAll();
            }
        }
        private void VisibleFalseAll()
        {

            label4.Visible = false;
            button5.Visible = false;
            label8.Visible = false;

            textBox3.Visible = false;
            label5.Visible = false;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
